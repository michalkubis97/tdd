package com.example.tdd.domain;

public class UserIndex {
  private Long indexNr;
  private final String userName;
  private final String userLastName;
  private final int userAge;

  public UserIndex(String userName, String userLastName, int userAge) {
    this.userName = userName;
    this.userLastName = userLastName;
    this.userAge = userAge;
  }

  public void setIndexNr(Long indexNr) {
    this.indexNr = indexNr;
  }

  public Long getIndexNr() {
    return indexNr;
  }

  public String getUserName() {
    return userName;
  }

  public String getUserLastName() {
    return userLastName;
  }

  public int getUserAge() {
    return userAge;
  }
}
