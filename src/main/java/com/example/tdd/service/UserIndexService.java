package com.example.tdd.service;

import com.example.tdd.domain.AddIndex;
import com.example.tdd.domain.UserIndexDto;

public interface UserIndexService {
  UserIndexDto addIndexForUser(AddIndex addIndex);
  UserIndexDto getIndex(Long indexNr);
}
