package com.example.tdd.domain;

public class AddIndex {
  private final String userName;
  private final String userLastName;
  private final int userAge;

  public AddIndex(String userName, String userLastName, int userAge) {
    this.userName = userName;
    this.userLastName = userLastName;
    this.userAge = userAge;
  }

  public String getUserName() {
    return userName;
  }

  public String getUserLastName() {
    return userLastName;
  }

  public int getUserAge() {
    return userAge;
  }
}
