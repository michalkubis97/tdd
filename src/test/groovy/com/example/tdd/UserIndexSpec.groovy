package com.example.tdd

import com.example.tdd.domain.AddIndex
import com.example.tdd.exception.WrongData
import com.example.tdd.service.UserIndexService
import com.example.tdd.service.impl.UserIndexServiceImpl
import spock.lang.Specification

class UserIndexSpec extends Specification {

  UserIndexService userIndexService = new UserIndexServiceImpl()

  def "should be able to add new index for user" () {
    when: "adds new index"
      def index = userIndexService.addIndexForUser(new AddIndex("Jan", "Nowak", 25))
    then: "index for user exists"
      userIndexService.getIndex(index.getIndexNr()).userLastName == index.userLastName
  }

  def "should not add index with wrong data" () {
    when: "tries to add index with wrong data"
      def index = userIndexService.addIndexForUser(null)
    then: "is not added"
      thrown(WrongData)
  }
  }

  

