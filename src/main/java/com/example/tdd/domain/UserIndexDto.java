package com.example.tdd.domain;

public class UserIndexDto {
  private final Long indexNr;
  private final String userName;
  private final String userLastName;
  private final int userAge;

  public UserIndexDto(Long indexNr, String userName, String userLastName, int userAge) {
    this.indexNr = indexNr;
    this.userName = userName;
    this.userLastName = userLastName;
    this.userAge = userAge;
  }

  public Long getIndexNr() {
    return indexNr;
  }

  public String getUserName() {
    return userName;
  }

  public String getUserLastName() {
    return userLastName;
  }

  public int getUserAge() {
    return userAge;
  }
}
