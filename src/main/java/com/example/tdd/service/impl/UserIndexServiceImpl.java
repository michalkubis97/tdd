package com.example.tdd.service.impl;

import com.example.tdd.domain.Office;
import com.example.tdd.domain.UserIndexDto;
import com.example.tdd.exception.WrongData;
import com.example.tdd.service.UserIndexService;
import com.example.tdd.domain.AddIndex;
import com.example.tdd.domain.UserIndex;

public class UserIndexServiceImpl implements UserIndexService {

  private final Office office;

  public UserIndexServiceImpl() {
    office = new Office();
  }

  @Override
  public UserIndexDto addIndexForUser(AddIndex addIndex) {
    if (addIndex == null) {
      throw new WrongData();
    }
    UserIndex userIndex = new UserIndex(addIndex.getUserName(), addIndex.getUserLastName(), addIndex.getUserAge());
    UserIndex saved = office.save(userIndex);
    return toDto(saved);
  }

  public UserIndexDto getIndex(Long indexNr) {
    UserIndex index = office.getByIndexNr(indexNr);
    return toDto(index);
  }

  private UserIndexDto toDto(UserIndex index) {
    return new UserIndexDto(index.getIndexNr(), index.getUserName(), index.getUserLastName(), index.getUserAge());
  }

}
