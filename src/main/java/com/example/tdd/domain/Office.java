package com.example.tdd.domain;

import java.util.HashMap;
import java.util.Map;

public class Office {

  private final Map<Long, UserIndex> indexes = new HashMap<>();
  private Long indexNr = 0L;


  public UserIndex save(UserIndex userIndex) {
    if (userIndex.getIndexNr() == null) {
      userIndex.setIndexNr(++indexNr);
    }
    indexes.put(indexNr, userIndex);
    return userIndex;
  }

  public UserIndex getByIndexNr(long indexNr) {
    return indexes.get(indexNr);
  }
}
